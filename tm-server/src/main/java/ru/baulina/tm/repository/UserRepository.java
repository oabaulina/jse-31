package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Autowired
    public UserRepository(@NotNull final EntityManager entityManager) {
        super(User.class, entityManager);
    }

    @Override
    public @Nullable User findUser(@NotNull String login, @NotNull String password) {
        @NotNull final String hsql = "SELECT t FROM User t WHERE t.login = :login AND t.passwordHash = :password";
        @NotNull final TypedQuery<User> query =
                entityManager.createQuery(hsql, User.class)
                        .setParameter("login", login)
                        .setParameter("password", password);
        return query.getResultList().get(0);
    }

    @Nullable
    @Override
    public User findById(@NotNull final Long id) {
        @NotNull final String sql = "SELECT t FROM User t WHERE t.id = :id";
        @NotNull final TypedQuery<User> query =
                entityManager.createQuery(sql, User.class)
                        .setParameter("id", id);
        return query.getResultList().get(0);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = "SELECT t FROM User t WHERE t.login = :login";
        @NotNull final TypedQuery<User> query = entityManager
                .createQuery(sql, User.class)
                .setParameter("login", login);
        @NotNull List<User> users = query.getResultList();
        if (users.isEmpty()) return null;
        return users.get(0);
    }

    @Override
    public void removeUser(@NotNull final User user) {
        @NotNull final Long id = user.getId();
        entityManager.remove(entityManager.find(User.class, id));
    }

    @Override
    public void removeById(@NotNull final Long id) {
        final User user = findById(id);
        if (user == null) return;
        removeUser(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return;
        removeUser(user);
    }

}
