package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Autowired
    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(Project.class, entityManager);
    }

    @Override
    public void remove(@NotNull final Project project) {
        @NotNull final Long id = project.getId();
        entityManager.remove(entityManager.find(Project.class, id));
    }

    @Override
    public void clear(@NotNull final Long userId) {
        @NotNull final List<Project> projects = findAll(userId);
        projects.forEach(entityManager::remove);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final Long userId) {
        @NotNull final String sql = "SELECT p FROM Project p WHERE p.user.id = :userId";
        @NotNull final TypedQuery<Project> query =
                entityManager.createQuery(sql, Project.class)
                        .setParameter("userId", userId);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Project> findListProjects() {
        @NotNull final String sql = "SELECT p FROM Project p";
        @NotNull final TypedQuery<Project> query =
                entityManager.createQuery(sql, Project.class);
        return query.getResultList();
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull final Long userId, @NotNull final Long id) {
        @NotNull final String sql = "SELECT p FROM Project p WHERE p.user.id = :userId AND p.id = :id";
        @NotNull final TypedQuery<Project> query =
                 entityManager.createQuery(sql, Project.class)
                        .setParameter("userId", userId)
                        .setParameter("id", id);
        return query.getResultList().get(0);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @NotNull final String sql = "SELECT p FROM Project p " +
                                    "WHERE p.user.id = :userId ORDER BY p.id";
        @NotNull final TypedQuery<Project> query =
                entityManager.createQuery(sql, Project.class)
                        .setParameter("userId", userId);
        return query.getResultList().get(index);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final Long userId, @NotNull final String name) {
        @NotNull final String sql = "SELECT p FROM Project p " +
                                    "WHERE p.user.id = :userId AND p.name = :name";
        @NotNull final TypedQuery<Project> query =
                entityManager.createQuery(sql, Project.class)
                        .setParameter("userId", userId)
                        .setParameter("name", name );
        return query.getResultList().get(0);
    }

    @Nullable
    @Override
    public Project removeOneById(@NotNull final Long userId, @NotNull final Long id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByIndex(@NotNull final Long userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneByName(@NotNull final Long userId, @NotNull final String name) {
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) return null;
        remove(project);
        return project;
    }

}
