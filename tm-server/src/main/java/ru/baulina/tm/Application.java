package ru.baulina.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.configuration.ApplicationConfig;


public class Application {

    public static void main(@NotNull final String[] args) throws Exception {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ApplicationConfig.class);
        context.registerShutdownHook();
        context.getBean(Bootstrap.class).run(args);
    }

}
