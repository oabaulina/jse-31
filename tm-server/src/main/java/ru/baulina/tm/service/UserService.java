package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.repository.IUserRepository;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Nullable
    @Override
    public List<User> findListUsers() {
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull List<User> listUsert = repository.findAll();
            repository.commitTransaction();
            return listUsert;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public User findUser(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable User user = repository.findUser(login, password);
            repository.commitTransaction();
            return user;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public User findById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable User user = repository.findById(id);
            repository.commitTransaction();
            return user;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable final User user = repository.findByLogin(login);
            repository.commitTransaction();
            return user;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        }
        return null;
    }

    @Override
    public void removeById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            repository.removeById(id);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            repository.removeByLogin(login);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeUser(@Nullable final User user) {
        if (user == null) return;
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            repository.removeUser(user);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
            repository.persist(user);
            repository.commitTransaction();
            return user;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull final User user = new User();
            user.setRole(Role.USER);
            user.setLogin(login);
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
            user.setEmail(email);
            repository.persist(user);
            repository.commitTransaction();
            return user;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable final User user = new User();
            user.setRole(role);
            user.setLogin(login);
            user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
            repository.persist(user);
            repository.commitTransaction();
            return user;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void lockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return;
            user.setLocked(true);
            repository.merge(user);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void unlockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable final User user = repository.findByLogin(login);
            if (user == null) return;
            user.setLocked(false);
            repository.merge(user);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void changePassword(
            @Nullable final String passwordOld,
            @Nullable final String passwordNew,
            @Nullable final Long userId
    ) {
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            final User user = findById(userId);
            if (user == null) throw new AccessDeniedException();
            final String hashOld = HashUtil.salt(passwordOld);
            if (hashOld == null) throw new AccessDeniedException();
            if (!hashOld.equals(user.getPasswordHash())) throw new AccessDeniedException();
            final String hashNew = HashUtil.salt(passwordNew);
            assert hashNew != null;
            user.setPasswordHash(hashNew);
            repository.merge(user);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void changeUser(
            @Nullable final String email,
            @Nullable final String festName,
            @Nullable final String LastName,
            @Nullable final Long userId
    ) {
        if (userId == null) throw new EmptyUserIdException();
        @Nullable final IUserRepository repository = getRepository();
        try {
            repository.beginTransaction();
            final User user = findById(userId);
            assert user != null;
            user.setEmail(email);
            user.setFirstName(festName);
            user.setLastName(LastName);
            repository.merge(user);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

}
