package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.TaskNotFoundException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;

import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Override
    protected ITaskRepository getRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Nullable
    @Override
    public Task create(
            @Nullable final User user,
            @Nullable final Project project,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (project == null) throw new EmptyProjectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull final Task task = new Task();
            task.setUser(user);
            task.setProject(project);
            task.setName(name);
            repository.merge(task);
            repository.commitTransaction();
            return task;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }
    
    @Nullable
    @Override
    public Task create(
            @Nullable final User user, @Nullable final Project project,
            @Nullable final String name, @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserException();
        if (project == null) throw new EmptyProjectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull final Task task = new Task();
            task.setUser(user);
            task.setProject(project);
            task.setName(name);
            task.setDescription(description);
            repository.merge(task);
            repository.commitTransaction();
            return task;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            repository.clear(userId);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull List<Task> listTask = repository.findAll(userId);
            repository.commitTransaction();
            return listTask;
        } catch (@NotNull final Exception e) {
           repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public List<Task> findListTasks() {
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull List<Task> listTask = repository.findListTasks();
            repository.commitTransaction();
            return listTask;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneById(
            @Nullable final Long userId, 
            @Nullable final Long projectId, 
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable Task task = repository.findOneById(userId, projectId, id);
            repository.commitTransaction();
            return task;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable Task task = repository.findOneByIndex(userId, projectId, index);
            repository.commitTransaction();
            return task;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByName(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable Task task = repository.findOneByName(userId, projectId, name);
            repository.commitTransaction();
            return task;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void remove(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyTaskException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            repository.remove(task);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable Task task = repository.removeOneById(userId, projectId, id);
            if (task == null) throw new TaskNotFoundException();
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable Task task = repository.removeOneByIndex(userId, projectId, index);
            if (task == null) throw new TaskNotFoundException();
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable Task task = repository.removeOneByName(userId, projectId, name);
            if (task == null) throw new TaskNotFoundException();
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void updateTaskById(
            @Nullable final Long userId, @Nullable final Long projectId,
            @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            final Task task = findOneById(userId, projectId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setId(id);
            task.setName(name);
            task.setDescription(description);
            repository.merge(task);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Long projectId,
            @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final ITaskRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @Nullable final Task task = findOneByIndex(userId, projectId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.merge(task);
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

}
