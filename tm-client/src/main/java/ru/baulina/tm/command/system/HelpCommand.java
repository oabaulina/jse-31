package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.SessionEndpoint;

import java.util.List;

@Component
public final class HelpCommand extends AbstractCommand {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final List<AbstractCommand> commands = commandService.findAll();
        for (final AbstractCommand command: commands) System.out.println(command);
        System.out.println("[OK]");
        System.out.println();
    }

}
