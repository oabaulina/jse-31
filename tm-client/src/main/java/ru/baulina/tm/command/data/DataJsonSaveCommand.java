package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;

@Component
public final class DataJsonSaveCommand extends AbstractDataCommand {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-json-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save json to binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataJasonSave(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
