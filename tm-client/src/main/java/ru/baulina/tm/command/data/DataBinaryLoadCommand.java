package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;

@Component
public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionDTOService sessionDTOService;

    @NotNull
    @Override
    public String name() {
        return "data-bin-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY LOAD]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataBinaryLoad(session);
        System.out.println("[LOGOUT CURRENT USER]");
        sessionEndpoint.closeSession(session);
        sessionDTOService.setSession(null);
        System.out.println("[OK]");
        System.out.println();
    }

}
