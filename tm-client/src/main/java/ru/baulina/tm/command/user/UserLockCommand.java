package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class UserLockCommand extends AbstractUserCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String name() {
        return "locked";
    }

    @NotNull
    @Override
    public String description() {
        return "Locked user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCKED_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        adminUserEndpoint.lockUserLogin(session, login);
        System.out.println("[OK]");
        System.out.println();
    }

}
