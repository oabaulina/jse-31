package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;

@Component
public final class DataBinaryClearCommand extends AbstractDataCommand {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-bin-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete bin data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY DELETE]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataBinaryClear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
