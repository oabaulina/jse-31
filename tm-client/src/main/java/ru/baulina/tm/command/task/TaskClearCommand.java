package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        @Nullable final SessionDTO session = getSession();
        taskEndpoint.clearTasks(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
