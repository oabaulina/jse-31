package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;

@Component
public class SessionCloseCommand extends AbstractUserCommand{

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "close-session";
    }

    @NotNull
    @Override
    public String description() {
        return "Close session.";
    }

    @Override
    public void execute() {
        System.out.println("[CLOSE_SESSION]");
        @Nullable final SessionDTO session = getSession();
        sessionEndpoint.closeSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
