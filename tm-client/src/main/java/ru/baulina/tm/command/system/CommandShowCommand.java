package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.List;

@Component
public final class CommandShowCommand extends AbstractCommand {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        @NotNull final List<String> commands = commandService.getCommandsNames();
        System.out.println(Arrays.toString(commands.toArray()));
        System.out.println();
    }

}
