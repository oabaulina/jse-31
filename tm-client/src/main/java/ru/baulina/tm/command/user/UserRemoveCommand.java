package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class UserRemoveCommand extends AbstractUserCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        adminUserEndpoint.removeUserByLogin(session, login);
        sessionEndpoint.closeAllSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
