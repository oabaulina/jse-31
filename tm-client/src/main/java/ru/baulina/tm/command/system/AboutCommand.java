package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.Session;

@Component
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println("OK");
        System.out.println();
    }

}
