package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.NumberUtil;
import ru.baulina.tm.util.SystemInformation;

@Component
public class ServerInfoCommand extends AbstractCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-iSer";
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about server.";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");

        @Nullable final SessionDTO session = getSession();
        System.out.println("HOST: " + adminUserEndpoint.getHost(session));
        System.out.println("PORT: " + adminUserEndpoint.getPort(session));
        System.out.println("[OK]");
        System.out.println();
    }


}
