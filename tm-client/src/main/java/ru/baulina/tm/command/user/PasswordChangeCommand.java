package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserEndpoint;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class PasswordChangeCommand extends AbstractUserCommand {

    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user's password.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE_PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        final String passwordOld = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        final String passwordNew = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        userEndpoint.changeUserPassword(session, passwordOld, passwordNew, session.getUserId());
        System.out.println("[OK]");
        System.out.println();
    }

}
