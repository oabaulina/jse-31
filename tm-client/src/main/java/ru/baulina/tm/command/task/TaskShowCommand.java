package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;

import java.util.List;

@Component
public final class TaskShowCommand extends AbstractTaskCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @Nullable final SessionDTO session = getSession();
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasks(session);
        int index = 1;
        for (TaskDTO task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
