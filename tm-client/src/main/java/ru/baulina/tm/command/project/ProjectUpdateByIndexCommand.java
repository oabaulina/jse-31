package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-update_by_index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index  = TerminalUtil.nexInt() -1;
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.removeOneProjectByIndex(session, index);
        System.out.println("ENTER NAME:");
        @Nullable final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description  = TerminalUtil.nextLine();
        projectEndpoint.updateProjectByIndex(session, index, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
