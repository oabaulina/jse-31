package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID");
        @Nullable final Long id = TerminalUtil.nexLong();
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.removeOneProjectById(session, id);
        System.out.println("[OK]");
        System.out.println();
    }

}
