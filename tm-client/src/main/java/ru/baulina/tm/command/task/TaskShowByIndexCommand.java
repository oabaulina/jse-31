package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.*;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX");
        @Nullable final Integer index = TerminalUtil.nexInt() -1;
        System.out.println("ENTER PROJECT ID:");
        @Nullable final Long projectId = TerminalUtil.nexLong();
        @Nullable final SessionDTO session = getSession();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByIndex(session, projectId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
