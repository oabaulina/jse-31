package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.util.NumberUtil;
import ru.baulina.tm.util.SystemInformation;

@Component
public final class InfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about system.";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");

        System.out.println("Available processors (cores): " + SystemInformation.getAvailableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(SystemInformation.getFreeMemory()));
        System.out.println("Maximum memory: " + NumberUtil.formatBytes(SystemInformation.getMaxMemory()));
        System.out.println(
                "Total memory available to JVM: " + NumberUtil.formatBytes(SystemInformation.getTotalMemory())
        );
        System.out.println(
                "Used memory by JVM: " + NumberUtil.formatBytes(SystemInformation.getUsedMemoryFormat())
        );
        System.out.println("OK");
        System.out.println();
    }

}
