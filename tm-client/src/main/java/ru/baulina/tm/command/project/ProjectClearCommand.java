package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.clearProjects(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
