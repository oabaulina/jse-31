package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserEndpoint;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class RegistryCommand extends AbstractUserCommand {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @NotNull
    @Override
    public String description() {
        return "Register new users.";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        final String email = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        userEndpoint.createUserWithEmail(session, login, password, email);
        System.out.println("[OK]");
        System.out.println();
    }

}
