package ru.baulina.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-update_by_index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index  = TerminalUtil.nexInt() -1;
        @Nullable final SessionDTO session = getSession();
        System.out.println("ENTER PROJECT ID:");
        @Nullable final Long projectId = TerminalUtil.nexLong();
        taskEndpoint.removeOneTaskByIndex(session, projectId, index);
        System.out.println("ENTER NAME:");
        @Nullable final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description  = TerminalUtil.nextLine();
        taskEndpoint.updateTaskByIndex(session, projectId, index, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
