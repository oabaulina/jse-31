package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.*;

import java.util.List;

@Component
public final class ProjectShowCommand extends AbstractProjectCommand {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task project.";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        @Nullable final SessionDTO session = getSession();
        @Nullable final List<ProjectDTO> projects = projectEndpoint.findAllProjects(session);
        int index = 1;
        for (ProjectDTO project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
