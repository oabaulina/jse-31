package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.Session;

import java.util.Arrays;
import java.util.List;

@Component
public final class ArgumentShowCommand extends AbstractCommand {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program arguments.";
    }

    @Override
    public void execute() {
        @NotNull final List<String> arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments.toArray()));
        System.out.println();
    }

}
