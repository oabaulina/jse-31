package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserDTO;
import ru.baulina.tm.util.TerminalUtil;

@Component
public final class ProfileOfUserCommand extends AbstractUserCommand {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String name() {
        return "profiler-of-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user's profiler.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW_PROFILE_OF_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        final UserDTO user = adminUserEndpoint.findUserByLogin(session, login);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FEST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
        System.out.println();
    }

}
