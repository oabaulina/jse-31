package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;

@Component
public final class DataJsonLoadCommand extends AbstractDataCommand {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionDTOService sessionDTOService;

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load json from binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataJasonLoad(session);
        System.out.println("[LOGOUT CURRENT USER]");
        sessionEndpoint.closeSession(session);
        sessionDTOService.setSession(null);
        System.out.println("[OK]");
        System.out.println();
    }

}
