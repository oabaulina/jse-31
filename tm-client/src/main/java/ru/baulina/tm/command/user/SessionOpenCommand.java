package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class SessionOpenCommand extends AbstractUserCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Autowired
    private ISessionDTOService sessionDTOService;

    @NotNull
    @Override
    public String name() {
        return "open-session";
    }

    @NotNull
    @Override
    public String description() {
        return "Open session.";
    }

    @Override
    public void execute() {
        System.out.println("[OPEN_SESSION]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final SessionDTO session = sessionEndpoint.openSession(login, password);
        sessionDTOService.setSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
