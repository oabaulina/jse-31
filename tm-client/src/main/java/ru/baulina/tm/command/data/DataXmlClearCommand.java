package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminDumpEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;

@Component
public final class DataXmlClearCommand extends AbstractDataCommand {

    @Autowired
    private AdminDumpEndpoint adminDumpEndpoint;

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete xml data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML DELETE]");
        @Nullable final SessionDTO session = getSession();
        adminDumpEndpoint.dataXmlClear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
