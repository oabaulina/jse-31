package ru.baulina.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;


import java.util.Scanner;

@UtilityClass
public class TerminalUtil {

    @NotNull
    private static final Scanner SCANNER = new Scanner(System.in);

    @NotNull
    public static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    public static Integer nexInt() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

    @NotNull
    public static Long nexLong() {
        @NotNull final String value = nextLine();
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

}
