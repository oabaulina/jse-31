package ru.baulina.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.baulina.tm.endpoint.*;

@Configuration
@ComponentScan("ru.baulina.tm")
public class ApplicationConfig {

    @NotNull
    @Bean
    public AdminDumpEndpointService adminDumpEndpointService() {
        return new AdminDumpEndpointService();
    }

    @NotNull
    @Bean
    public AdminDumpEndpoint adminDumpEndpoint(
            @NotNull final AdminDumpEndpointService adminDumpEndpointService
    ) {
        return adminDumpEndpointService.getAdminDumpEndpointPort();
    }

    @NotNull
    @Bean
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @NotNull
    @Bean
    public AdminUserEndpoint adminUserEndpoint(
            @NotNull final AdminUserEndpointService adminUserEndpointService
    ) {
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

    @NotNull
    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @NotNull
    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @NotNull
    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @NotNull
    @Bean
    public SessionEndpoint sessionEndpoint(
            @NotNull final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @NotNull
    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @NotNull
    @Bean
    public TaskEndpoint taskEndpoint(
            @NotNull final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

    @NotNull
    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @NotNull
    @Bean
    public UserEndpoint userEndpoint(
            @NotNull final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

}
