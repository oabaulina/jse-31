package ru.baulina.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.exception.system.UnknownCommandException;
import ru.baulina.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private List<AbstractCommand> commands;

    @NotNull
    @Autowired
    private ICommandService commandService;

    private void initCommands(@NotNull final List<AbstractCommand> commands) {
        for (@NotNull AbstractCommand command: commands) {
            commandService.add(command);
        }
    }

    public void run(final String[] args) throws Exception {
        initCommands(commands);
        System.out.println("** Welcome to task manager **");
        System.out.println();
        try {
            if (parseArgs(args)) System.exit(0);
        } catch (Exception e) {
            logError(e);
            System.exit(0);
        }
        process();
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                logError(e);
            }
        }
    }

    private static void logError(Exception e) {
        System.err.println(e.getMessage());
        System.err.println("[FAIL]");
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        return parseArg(arg);
    }

    private boolean parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        @Nullable final AbstractCommand command = commandService.getByArg(arg);
        if (command == null) throw new UnknownCommandException(arg);
        command.execute();
        return true;
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

}
