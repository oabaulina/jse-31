package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    void add(@Nullable final AbstractCommand command);

    void remove(@Nullable final AbstractCommand command);

    @NotNull
    List<AbstractCommand> findAll();

    @NotNull
    List<String> getCommandsNames();

    @NotNull
    List<String> getArgs();

    void clear();

    @Nullable
    AbstractCommand getByArg(@Nullable final String arg);

    @Nullable
    AbstractCommand getByName(@Nullable final String name);

    @Nullable
    AbstractCommand removeByName(@Nullable final String name);

    @Nullable
    AbstractCommand removeByArg(@Nullable final String name);

}
