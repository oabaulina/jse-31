package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.exception.system.EmptyArgumentException;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private final ICommandRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public void remove(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.remove(command);
    }

    @NotNull
    @Override
    public List<AbstractCommand> findAll() {
        return commandRepository.findAll();
    }

    @NotNull
    @Override
    public List<String> getCommandsNames() {
        return commandRepository.getCommandsNames();
    }

    @NotNull
    @Override
    public List<String> getArgs() {
        return commandRepository.getArgs();
    }

    @Override
    public void clear() {
        commandRepository.clear();
    }

    @Nullable
    @Override
    public AbstractCommand getByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.getByArg(arg);
    }

    @Nullable
    @Override
    public AbstractCommand getByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.getByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand removeByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.removeByName(name);
    }

    @Nullable
    @Override
    public AbstractCommand removeByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.removeByArg(arg);
    }

}
